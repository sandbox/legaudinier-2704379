<?php
/**
 * Implements hook_form_system_theme_settings_alter()
 */
function hitchhiker_form_system_theme_settings_alter(&$form, &$form_state) {
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  $toggle_welcome = theme_get_setting('hitchhiker_toggle_welcome', $theme);
  if (is_null($toggle_welcome)) {
    $toggle_welcome = TRUE;
  }

  $form['welcome'] = array(
    '#type' => 'details',
    '#attributes' => array('class' => array('welcome', 'hitchhiker-help')),
    '#title' => t('Welcome to Hitchhiker'),
    '#weight' => -1,
    '#open' => $toggle_welcome,
    '#tree' => FALSE,
  );

  $logo = base_path() . drupal_get_path('theme', 'hitchhiker') . '/logo.svg';

  $form['welcome']['hitchhiker'] = array(
    '#prefix' => '<div class="hitchhiker-welcome">',
    '#markup' => '<img src="' . $logo . '" />',
    '#suffix' => '</div>',
  );

  $form['welcome']['hitchhiker']['#markup'] .= t('<h3>Hitchhiker</h3>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<p><strong>Project Page</strong> - <a href="https://www.drupal.org/sandbox/legaudinier/2704379" target="_blank">https://www.drupal.org/sandbox/legaudinier/2704379</a>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<p><strong>Issue Queue</strong> - <a href="https://www.drupal.org/project/issues/2704379" target="_blank">https://www.drupal.org/project/issues/2704379</a>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<p>The following modules are recommended:</p>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<ul>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<li><strong>Hitchiker Tools</strong> - Included in the Hitchhiker theme. Embeds the KSS automated styleguide into the theme at admin/appearance/design/hitchhiker.</li>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<li><strong>Display Suite</strong> - <a href="http://drupal.org/project/ds" target="_blank">drupal.org/project/ds</a></li>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<li><strong>Fences</strong> - <a href="http://drupal.org/project/fences" target="_blank">drupal.org/project/fences</a></li>');
  $form['welcome']['hitchhiker']['#markup'] .= t('<li><strong>Style guide</strong> - <a href="https://www.drupal.org/project/styleguide">drupal.org/project/styleguide</a></li>');
  $form['welcome']['hitchhiker']['#markup'] .= t('</ul>');

  $form['hitchhiker_toggle_welcome'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show this welcome section by default'),
    '#description' => t(''),
    '#default_value' => $toggle_welcome,
    '#group' => 'welcome',
  );
}
