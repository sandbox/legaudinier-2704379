<?php
namespace Drupal\hitchhiker_tools\Theme;

use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Switches to the Hitchhiker theme for the design_component_library route.
 */
class HitchhikerToolsNegotiator implements ThemeNegotiatorInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'hitchhiker_tools.design_component_library';
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return 'hitchhiker';
  }

}
